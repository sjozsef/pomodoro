Pomodoro v0.1 by Samu József (sjozsef@sanapse.hu)

This is an Ubuntu indicator to help you working with Pomodoro technique. 
More about the Pomodoro techique: http://www.pomodorotechnique.com/

*************************************************************************
Please note, this is a very early version of this project!
*************************************************************************

================
INSTALL
================

1. Extract the downloaded file
2. Open a terminal and run: sudo sh <path-to-extracted-files>/install.sh
3. Start Pomodoro and work!


================
TODO
================

- Configurable time intervals
- Stats
- Prevent launching multiple instances
- Block some websites if you need to work