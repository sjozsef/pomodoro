# minimal pomodoro indicator for ubuntu
# written by Samu Jozsef
# based on Charl P. Botha's IndicatorCPUSpeed (copyright 2012 Charl P. Botha <info@charlbotha.com>)
# Copyright Samu Jozsef 2013
# hereby released under the BSD license

# use the PyGObject GObject introspection to use GTK+ 3 
# also see
# http://readthedocs.org/docs/python-gtk-3-tutorial/en/latest/index.html
# http://developer.gnome.org/gtk3/stable/ (API reference)

# v0.1

from gi.repository import Gtk, GLib, Notify

try: 
       from gi.repository import AppIndicator3 as AppIndicator  
except:  
       from gi.repository import AppIndicator

import re
import subprocess

class Pomodoro:
    def __init__(self):

        self.reset()

        Notify.init ('Pomodoro')
        self.work_not = Notify.Notification.new ('Go to work!', 'Let\'s go and keep focused!', 'dialog-information')
        self.sbreak_not = Notify.Notification.new ('Take a break!', 'Okay, you\'re awsome! You deserve a little break. :)', 'dialog-information')
        self.lbreak_not = Notify.Notification.new ('Take a break!', 'Okay, you\'re awsome! You deserve a longer break. :)', 'dialog-information')

        self.ind=AppIndicator.Indicator.new(
                            "indicator-pomodoro",
                            "",
                            AppIndicator.IndicatorCategory.APPLICATION_STATUS)

        self.ind.set_status (AppIndicator.IndicatorStatus.ACTIVE)

        # have to give indicator a menu
        self.menu = Gtk.Menu()

        # Start/Pause 
        self.start = Gtk.MenuItem()
        self.start.set_label("Start")
        self.start.connect("activate", self.handler_menu_start)
        self.start.show()
        self.menu.append(self.start)

        # reset everything
        self.resetbtn = Gtk.MenuItem()
        self.resetbtn.set_label("Reset")
        self.resetbtn.connect("activate", self.handler_menu_reset)
        self.resetbtn.show()
        self.menu.append(self.resetbtn)

        # this is for exiting the app
        self.quit = Gtk.MenuItem()
        self.quit.set_label("Exit")
        self.quit.connect("activate", self.handler_menu_exit)
        self.quit.show()
        self.menu.append(self.quit)

        self.menu.show()
        self.ind.set_menu(self.menu)

        self.update()
        # then start updating every 2 seconds
        # http://developer.gnome.org/pygobject/stable/glib-functions.html#function-glib--timeout-add-seconds
        GLib.timeout_add_seconds(1, self.handler_timeout)

    def reset(self):
        self.work_mins=25
        self.sbreak_mins=5
        self.lbreak_mins=20
        self.sbreaks_num=4
        self.current_state="paused"
        self.last_state="work"
        self.current_secs=self.work_mins*60
        self.current_sbreaks=0

    def play_sound(self):
        subprocess.call(['/usr/bin/canberra-gtk-play','--id','system-ready'])

    def to_two_num(self, s):
        if(len(s)==1):
            return '0'+s
        else:
            return s

    def get_time_left(self):
        m=self.current_secs/60
        s=self.current_secs%60
        strm=self.to_two_num(str(m))
        strs=self.to_two_num(str(s))
        return strm+':'+strs

    def get_state_letter(self):
        s='W'
        if(self.current_state=='paused'):
            s='P'
        elif(self.current_state=='sbreak' or self.current_state=='lbreak'):
            s='B'
        return s

    def next_state(self):
        self.play_sound()
        if(self.current_state=='work'):
            if(self.current_sbreaks == self.sbreaks_num):
                self.current_sbreaks=0
                self.current_state='lbreak'
                self.current_secs=self.lbreak_mins*60
                self.lbreak_not.show()
            else:
                self.current_state='sbreak'
                self.current_secs=self.sbreak_mins*60
                self.sbreak_not.show()
        elif(self.current_state=='sbreak'):
            self.current_state='work'
            self.current_secs=self.work_mins*60
            self.work_not.show()
            self.current_sbreaks += 1
        elif(self.current_state=='lbreak'):
            self.current_state='work'
            self.current_secs=self.work_mins*60
            self.lbreak_not.show()
        self.update()

    def handler_menu_exit(self, evt):
        Gtk.main_quit()

    def handler_menu_start(self, evt):
        if(self.current_state == "paused"):
            self.start.set_label('Pause')
            self.current_state=self.last_state
            self.last_state="paused"
            self.work_not.show()
            self.play_sound()
        else:
            self.last_state=self.current_state
            self.current_state='paused'
            self.start.set_label('Resume')
        self.update()

    def handler_menu_reset(self, evt):
        self.reset()
        self.update()

    def handler_timeout(self):
        """This will be called every seconds by the GLib.timeout.
        """
        if(self.current_state != 'paused'):
            self.current_secs-=1
            if(self.current_secs==0):
                self.next_state()
        self.update()
        # return True so that we get called again
        # returning False will make the timeout stop
        return True

    def update(self):
        t=self.get_time_left()
        s=self.get_state_letter()
        self.ind.set_label(s + ' | ' + t, "")

    def main(self):
        Gtk.main()

if __name__ == "__main__":
    ind = Pomodoro()
    ind.main()