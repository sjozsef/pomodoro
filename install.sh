#!/bin/bash

SCRIPT=$(readlink -f "$0")
SCRIPTPATH=`dirname "$SCRIPT"`

echo "Installing Pomodoro..."
mkdir /usr/share/pomodoro
mkdir /usr/bin/pomodoro
cp $SCRIPTPATH"/pomodoro.py" "/usr/bin/pomodoro/pomodoro.py"
cp $SCRIPTPATH"/pomodoro.desktop.txt" "/usr/share/applications/pomodoro.desktop"
cp $SCRIPTPATH"/tomato.png" "/usr/share/pomodoro/tomato.png"
echo "Installation succeeded!" 